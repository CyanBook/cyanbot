## What is CyanBot?
**CyanBot** is a framework for [Telegram Bots](https://core.telegram.org/bots) entirely built in PHP by CyanBook

## Why should you use CyanBot?
**CyanBot has a lot of different functions improving the creation of your bot!**

##### Here's some examples
* You can add a password to authenticate your requests
* Every method is supported
* Dev mode pre-installed

## System Requirements
- VPS:
* Ubuntu 16+
* Nginx (Recommended) + Domain with SSL
* PHP7 & cURL
* MySQL

-----------------

- Hosting:
* Domain with SSL
* PHP7
* MySQL

**Functioning with PHP5 and/or other distros is not guaranteed**

## Installation
* Executing `apt install git && git clone https://gitlab.com/CyanBook/cyanbot` on a bash shell
* Downloading the ZIP file from the [repository](https://gitlab.com/CyanBook/cyanbot)

## Official channel
- [Click Here](https://t.me/CyanBotFramework)

## Official group
- *Coming soon..*

## Official guide
- *Coming soon..*

 
© 2019 CyanBook